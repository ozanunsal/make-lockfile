#!/bin/sh

podman build -t test-make-lockfile:latest tests
podman run --privileged -v $(pwd):/make-lockfile -it test-make-lockfile:latest /bin/bash -c \
  "cd /make-lockfile && PYTHONPATH="${PYTHONPATH}:/make-lockfile" pytest -v tests"
#podman run --privileged -v $(pwd):/make-lockfile -it test-make-lockfile:latest /bin/bash
